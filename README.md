# Explorador de API de Rick & Morty - Prueba Tecnica
Versión alfa 1

El explorador de API de Rick y Morty es una aplicación web desarrollada utilizando para el Frontend React y para el Backend Laravel 8 que emula la API pública de Rick y Morty utilizando SQLite, y agrega ejemplos de usabilidad, navegación, consulta y gestión CRUD.

Esta aplicación se desarrolla con el objeto de dar solución a la evaluación técnica para la búsqueda de desarrolladores senior fullstack en Datatraffic.

Se utilizó una arquitecura Cliente / Servidor básica con un desarrollo basado en componentes CDD.

## INSTALACION
- git clone rm-prueba-tecnica
- cd rm-prueba-tecnica

- ### FRONTEND
    - cd ./client
    - npm install

- ### BACKEND
    - cd ./server
    - composer install
    - cp .env.example .env
    - php artisan key:generate

## COMPILACIÓN FRONTEND
- Para compilar React (cuando haya sido modificado) se debe ejecutar npm run build en ./client

## EJECUCION
- ### FRONTEND
    - cd ./client
    - npm run start:dev

- ### BACKEND
    - cd ./server
    - php artisan serve --port=1995

## CAPTURAS
![](./_README/1.png)
![](./_README/2.png)
![](./_README/3.png)
![](./_README/8.png)
![](./_README/4.png)
![](./_README/5.png)
![](./_README/6.png)
![](./_README/7.png)

## FEATURES
- El backend expone API para manejo de CRUD de personajes, episodios, y lugares
- Las entidades personajes, episodios y lugares, existen localmente en una base de datos SQLite


## STACK UTILIZADO
- Laravel
- Webpack
- React JS
- React Router
- Bootstrap
- Bootswatch
- SQLite
- Git y npm

## AUTOR
- José Daian Cabrera Rios (@josedaian)


## FECHA DE ÚLTIMA ACTUALIZACIÓN
21/06/2021

por @josedaian