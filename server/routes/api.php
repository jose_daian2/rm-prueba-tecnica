<?php

use App\Http\Controllers\CharacterController;
use App\Http\Controllers\EpisodeController;
use App\Http\Controllers\LocationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

#Character
Route::get('/characters', [CharacterController::class, 'index']);
Route::post('/characters', [CharacterController::class, 'store']);
Route::get('/characters/{character}', [CharacterController::class, 'detail']);
Route::match(['PUT', 'PATCH'], '/characters/{character}', [CharacterController::class, 'update']);
Route::delete('/characters/{character}', [CharacterController::class, 'delete']);

#Location
Route::get('/locations', [LocationController::class, 'index']);
Route::post('/locations', [LocationController::class, 'store']);
Route::get('/locations/{location}', [LocationController::class, 'detail']);
Route::match(['PUT', 'PATCH'], '/locations/{location}', [LocationController::class, 'update']);
Route::delete('/locations/{location}', [LocationController::class, 'delete']);

#Episode
Route::get('/episodes', [EpisodeController::class, 'index']);
Route::post('/episodes', [EpisodeController::class, 'store']);
Route::get('/episodes/{episode}', [EpisodeController::class, 'detail']);
Route::match(['PUT', 'PATCH'], '/episodes/{episode}', [EpisodeController::class, 'update']);
Route::delete('/episodes/{episode}', [EpisodeController::class, 'delete']);
