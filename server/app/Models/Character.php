<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    /** @var false */
    public $timestamps = false;
    /** @var string[] */
    protected $fillable = [
        'name', 'status', 'species', 'type', 'gender', 'origin', 'location', 'image', 'episode', 'url', 'created'
    ];

    /** @var string[] */
    protected $casts = [
        'origin' => 'array',
        'location' => 'array',
        'episode' => 'array',
    ];
}
