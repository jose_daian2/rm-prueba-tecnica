<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    /** @var false */
    public $timestamps = false;
    /** @var string[] */
    protected $fillable = [
        'name', 'air_date', 'episode', 'characters', 'url', 'created'
    ];

    /** @var string[] */
    protected $casts = [
        'characters' => 'array',
    ];
}
