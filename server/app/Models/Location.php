<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /** @var false */
    public $timestamps = false;
    /** @var string[] */
    protected $fillable = [
        'name', 'type', 'dimensions', 'residents', 'url', 'created'
    ];

    /** @var string[] */
    protected $casts = [
        'residents' => 'array',
    ];
}
