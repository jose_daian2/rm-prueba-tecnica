<?php

namespace App\Http\Controllers;

use App\Http\Requests\CharacterRequest;
use App\Http\Resources\CharacterCollection;
use App\Http\Resources\CharacterResource;
use App\Models\Character;
use App\Traits\ApiResponder as ApiResponder;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Container\BindingResolutionException;

class CharacterController extends Controller
{
    use ApiResponder;
    
    /**
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function index(){
        return new CharacterCollection(Character::paginate());
    }

    /**
     * @param Character $character 
     * @return CharacterResource|JsonResponse 
     * @throws BindingResolutionException 
     */
    public function detail(Character $character){
        try{
            return new CharacterResource($character);
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param CharacterRequest $request 
     * @return CharacterResource 
     */
    public function store(CharacterRequest $request){
        try{
            return new CharacterResource(Character::create($request->all()));
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param CharacterRequest $request 
     * @return CharacterResource|JsonResponse 
     * @throws BindingResolutionException 
     */
    public function update(CharacterRequest $request, Character $character){
        try{
            $character->fill($request->all())->save();
            return new CharacterResource($character);
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param Character $character 
     * @return CharacterResource|JsonResponse|void 
     * @throws BindingResolutionException 
     */
    public function delete(Character $character){
        try{
            $character->delete();
            return $this->successResponse('Registro eliminado');
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }
}
