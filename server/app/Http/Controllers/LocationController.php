<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationRequest;
use App\Http\Resources\LocationCollection;
use App\Http\Resources\LocationResource;
use App\Models\Location;
use App\Traits\ApiResponder as ApiResponder;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Container\BindingResolutionException;

class LocationController extends Controller
{
    use ApiResponder;
    
    /**
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function index(){
        return new LocationCollection(Location::paginate());
    }

    /**
     * @param Location $location 
     * @return LocationResource|JsonResponse 
     * @throws BindingResolutionException 
     */
    public function detail(Location $location){
        try{
            return new LocationResource($location);
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param LocationRequest $request 
     * @return LocationResource 
     */
    public function store(LocationRequest $request){
        try{
            return new LocationResource(Location::create($request->all()));
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param LocationRequest $request 
     * @return LocationResource|JsonResponse 
     * @throws BindingResolutionException 
     */
    public function update(LocationRequest $request, Location $location){
        try{
            $location->fill($request->all())->save();
            return new LocationResource($location);
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param Location $location 
     * @return LocationResource|JsonResponse|void 
     * @throws BindingResolutionException 
     */
    public function delete(Location $location){
        try{
            $location->delete();
            return $this->successResponse('Registro eliminado');
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }
}
