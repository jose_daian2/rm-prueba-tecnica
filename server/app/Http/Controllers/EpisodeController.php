<?php

namespace App\Http\Controllers;

use App\Http\Requests\EpisodeRequest;
use App\Http\Resources\EpisodeCollection;
use App\Http\Resources\EpisodeResource;
use App\Models\Episode;
use App\Traits\ApiResponder as ApiResponder;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Container\BindingResolutionException;

class EpisodeController extends Controller
{
    use ApiResponder;
    
    /**
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function index(){
        return new EpisodeCollection(Episode::paginate());
    }

    /**
     * @param Episode $episode 
     * @return EpisodeResource|JsonResponse 
     * @throws BindingResolutionException 
     */
    public function detail(Episode $episode){
        try{
            return new EpisodeResource($episode);
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param EpisodeRequest $request 
     * @return EpisodeResource 
     */
    public function store(EpisodeRequest $request){
        try{
            return new EpisodeResource(Episode::create($request->all()));
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param EpisodeRequest $request 
     * @return EpisodeResource|JsonResponse 
     * @throws BindingResolutionException 
     */
    public function update(EpisodeRequest $request, Episode $episode){
        try{
            $episode->fill($request->all())->save();
            return new EpisodeResource($episode);
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }

    /**
     * @param Episode $episode 
     * @return EpisodeResource|JsonResponse|void 
     * @throws BindingResolutionException 
     */
    public function delete(Episode $episode){
        try{
            $episode->delete();
            return $this->successResponse('Registro eliminado');
        }catch(Exception $exception){
            \Log::error(__METHOD__, ['exception' => $exception]);
            return $this->errorResponse("Ha ocurrido un error interno. Intentelo de nuevo por favor.");
        }
    }
}
