<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EpisodeCollection extends ResourceCollection
{
    public static $wrap = 'results';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'info' => [
                'count' => $this->resource->total(),
                'pages' => $this->resource->lastPage()
            ],
            'results' => $this->collection,
        ];
    }
}
