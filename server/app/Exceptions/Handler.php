<?php

namespace App\Exceptions;

use App\Traits\ApiResponder;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiResponder;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param Request $request 
     * @param Throwable $exception 
     * @return Response 
     * @throws BindingResolutionException 
     */
    public function render($request, Throwable $exception){
        return $this->handleException($request, $exception);
    }

    /**
     * @param $request
     * @param $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleException($request, $exception)
    {
        if( $exception instanceof \Throwable ){
            $exceptionMessage = $exception->getMessage();
            if ($exception instanceof MethodNotAllowedHttpException) {
                $exceptionMessage = 'El método especificado para la solicitud no es válido.';
            }
            
            if ($exception instanceof NotFoundHttpException) {
                
                $exceptionMessage = 'Recurso no existe ';
            }
            
            if ($exception instanceof QueryException) {
                $exceptionMessage = 'Error de base de datos';
            }
            
            return $this->errorResponse($exceptionMessage);
        }

        return $this->errorResponse('Excepción no controlada');
    }
}
