<?php

namespace App\Traits;

use Throwable;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

trait ApiResponder
{
    /**
     * @param string $message
     * @param string $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($message = '', $code = 0, $code_http = Response::HTTP_INTERNAL_SERVER_ERROR): JsonResponse {
        $response = [
            'msg' => $message,
            'error' => true
        ];
        return response()->json($response, $code_http);
    }

    /**
     * @param array $data
     * @param string $message
     * @param string $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($data = [], $message = 'Petición exitosa', $code = 0, $code_http = Response::HTTP_OK)
    {
        $response = [
            'msg' => $message,
            'error' => false
        ];

        $headers = [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
        ];

        return response()->json($response, $code_http, $headers);
    }
    
}
