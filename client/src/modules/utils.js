import React from "react";

const utils = {
  loremRandomWord: () => {
    let words =
      "Lorem ipsum dolor sit amet consectetur adipiscing elit In sit amet massa orci Ut commodo ligula sapien ultricies blandit mauris".split(
        " "
      );
    return words[~~(Math.random() * words.length)];
  },
  validatePostFields: (data) => {
    for (var x in data) {
      try {
        data[x] = JSON.parse(data[x]);
      } catch (e) {
        data[x] = data[x];
      }
    }
    return data;
  },
};

export default utils;
